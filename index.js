var calculatorApp = angular.module('calculatorApp', []);

calculatorApp.component('calculatorButton',{
  template: '<button ng-click="$ctrl.buttonClicked($ctrl.value)">{{$ctrl.value}}</button>',
  bindings: {
    value : "<"
  },
  controller: function HiThereController($scope){
    this.buttonClicked = function(value){
      var stx = value;
      $scope.$emit("button-action", stx);
    }
  }
});

calculatorApp.controller('CalculatorController', function CalculatorController($scope) {
  $scope.valueButton1 = 1;
  $scope.valueButton2 = 2;
  $scope.valueButton3 = 3;
  $scope.valueButton4 = 4;
  $scope.valueButton5 = 5;
  $scope.valueButton6 = 6;
  $scope.valueButton7 = 7;
  $scope.valueButton8 = 8;
  $scope.valueButton9 = 9;
  $scope.valueButton0 = 0;

  $scope.valueButtonEq = "=";
  $scope.valueButtonMin = "-";
  $scope.valueButtonPlus = "+";
  $scope.valueButtonMulti = "*";
  $scope.valueButtonDiv = "\\";
  
  $scope.currentOutputState = '';
  $scope.firstParameter = '';
  $scope.secondParameter = '';
  $scope.currentOperation = 'no op'
  $scope.filingSecondParameter = false;
  $scope.carryOver = false;

  $scope.$on("button-action", function(evt, passValue){
      if(isNaN(passValue)){
        if(passValue === '='){
          var first = parseInt($scope.firstParameter);
          var second = parseInt($scope.secondParameter);

          var result = 0;
          switch($scope.currentOperation){
            case '+': result = first + second; break;
            case '-': result = first - second; break;
            case '*': result = first * second; break;
            case '/': result = first / second; break;
          }

          $scope.currentOutputState = result;
          $scope.firstParameter = ''+result;
          $scope.secondParameter = '';
          $scope.filingSecondParameter = false;
          $scope.carryOver = true;
          $scope.currentOperation = 'noop';
        } else{
          $scope.filingSecondParameter = true;
          $scope.currentOperation = passValue;
        }
      } else{
        if($scope.filingSecondParameter){
          $scope.secondParameter = $scope.secondParameter + passValue;
          $scope.currentOutputState = $scope.secondParameter;
        } else{
          if($scope.carryOver){
            $scope.firstParameter = '' + passValue;
            $scope.carryOver = false;
          } else{
            $scope.firstParameter = $scope.firstParameter + passValue;
          }
          $scope.currentOutputState = $scope.firstParameter;
        }
      }
  });
});